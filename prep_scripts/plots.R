theme_set(camiller::theme_din(base_family = "Barlow Semi Condensed"))

make_front_map <- function(nm) {
  cwi::town_sf %>%
    mutate(lbl = forcats::fct_other(name, keep = nm, other_level = "Other towns")) %>%
    ggplot() +
    geom_sf(aes(fill = lbl), color = "white", size = 0.3) +
    scale_fill_manual(values = c("mediumorchid", "gray70")) +
    theme_void()
}

make_demo_table <- function(nm) {
  read_csv(here::here("input_data/demographics/pop_by_race_6grps_2019.csv")) %>%
    filter(name == nm) %>%
    knitr::kable()
}

make_income_chart <- function(nm) {
  read_csv(here::here("input_data/income/median_income_race_2019.csv")) %>%
    filter(name == nm) %>%
    ggplot(aes(x = race, y = minc)) +
    geom_col()
}
